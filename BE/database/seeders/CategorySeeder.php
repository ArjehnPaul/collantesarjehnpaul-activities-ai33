<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [ 'Math', 'Science', 'History', 'English', ];

        foreach($categories as $catergory) {
            Category::create(['category' => $catergory]);
        }
    }
}

