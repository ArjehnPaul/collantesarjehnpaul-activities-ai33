<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class StorePatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'required|min:2|max:255', 
            'first_name' => 'required|min:2|max:255',
            'middle_name' => 'required|min:2|max:255',
            'email' => 'required|unique:patrons|email'
        ];
    }
    public function message()
    {
        return [        
            'last_name.required' => 'Last Name is required.',
            'last_name.min' => 'Last Name must have 2 characters',
            'last_name.max' => 'Last Name must not exceed 100 characters',
            'first_name.required' => 'First Name is required.',
            'first_name.min' => 'First Name must have 2 characters',
            'first_name.max' => 'First Name must not exceed 100 characters',
            'middle_name.required' => 'Middle Name is required.',
            'middle_name.min' => 'Middle Name must have 2 characters',
            'middle_name.max' => 'Middle Name must not exceed 100 characters',
            'email.required' => 'Last Name is required.',
            'email.unique' => 'Email already exists',
        ];
    }
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
