<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\BorrowedBook;
use App\Http\Requests\StoreBorrowedBookRequest;

class BorrowedBookController extends Controller
{
    public function index()
    {
        return response()->json(BorrowedBook::with('patron')->get());
    }
    public function store(StoreBorrowedBookRequest $request)
    { 
        $create_borrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
        
        $borrowedbook = BorrowedBook::with(['book', 'patron'])->find($create_borrowed->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);

        return response()->json(['message' => 'Book borrowed successfully', 'borrowed' => $borrowedbook]);
    }
    public function show($id)
    {
        return response()->json(Books::findOrFail($id));
    }

}
