<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Patron;
use App\Http\Requests\StorePatronRequest;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    public function index()
    {
        return response()->json(Patron::all());
    }
    public function show($id)
    {
        return response()->json(Patron::findOrFail($id));
    }
    public function store(StorePatronRequest $request)
    {
        Patron::create($request->validated());
        return response()->json(['message' => 'Added Patron.'], 201);
    }
    public function update(StorePatronRequest $request, $id)
    {

        Patron::where('id', $id)->update($request->validated());
        return response()->json(['message' => 'Updated Patron.']);
    }
    public function destroy($id)
    {
        Patron::where('id', $id)->delete();
        return response()->json(['message' => 'Deleted Patron.']);
    }
}
