<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Http\Requests\StoreReturnedBookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    public function index()
    {
        return response()->json(ReturnedBook::all());
    }
    public function store(StoreReturnedBookRequest $request)
    {
                //Retrieve first the borrowed book
                try {
                    $borrowedbook = BorrowedBook::where([
                        ['book_id', $request->book_id],
                        ['patron_id', $request->patron_id],
                    ])->firstOrFail();
                } catch (ModelNotFoundException $exception) {
                    return response()->json(['message' => 'Book not found'], 404);
                }
        
                if (!empty($borrowedbook)) 
                {
                    if ($borrowedbook->copies == $request->copies) {
                        $borrowedbook->delete();
                    } else {
                        $borrowedbook->update(['copies' => $borrowedbook->copies - $request->copies]);
                    }
        
                    $create_returned = ReturnedBook::create($request->only(['book_id', 'copies', 'patron_id']));
                    $returnedbook = ReturnedBook::with(['book'])->find($create_returned->id);
                    $copies = $returnedbook->book->copies + $request->copies;
        
                    $returnedbook->book->update(['copies' => $copies]);
                    return response()->json(['message' => 'Book returned successfully!', 'book' => $returnedbook]);
                }
    }
    public function show($id)
    {
        return response()->json(Books::findOrFail($id));
    }
    public function destroy($id)
    {
        BorrowedBook::where('patron_id', $id)->delete();
    }

}
