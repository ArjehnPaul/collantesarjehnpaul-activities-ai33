<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Http\Requests\StoreBookRequest;

class BookController extends Controller
{
    public function index()
    {
        return response()->json(Books::all());
    }
    public function show($id)
    {
        return response()->json(Books::findOrFail($id));
    }
    public function store(StoreBookRequest $request)
    {
        $book = Books::create($request->validated());
        return response()->json(['message' => 'Added Book.', 'book' => $book], 201);
    }
    public function update(StoreBookRequest $request, $id)
    {
        Books::where('id', $id)->update($request->validated());
        return response()->json(['message' => 'Updated Book.', 'book' => Books::where('id', $id)->first()]);
    }
    public function destroy($id)
    {
        Books::where('id', $id)->delete();
        return response()->json(['message' => 'Deleted Book.']);
    }
}
