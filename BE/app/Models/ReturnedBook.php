<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnedBook extends Model
{
    use HasFactory;

    protected $fillable = ['book_id', 'copies', 'patron_id'];
    //patron
    public function patron() {
        return $this->belongsTo(Patron::class, 'patron_id', 'id');
    }
    //books
    public function book() {
        return $this->belongsTo(Books::class, 'book_id', 'id');
    }
}
