import Vue from 'vue'
import router from './components'
import App from './App.vue'
import toastr from "@/assets/js/toastr"
import store from "./store";

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Toast from "vue-toastification";

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-toastification/dist/index.css";
import "toastr/build/toastr.css";

Vue.config.productionTip = false;
Vue.use(toastr);
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
