import Vue from 'vue'
import VueRouter from 'vue-router'
import dashboard from '@/components/router/Dashboard'


Vue.use(VueRouter)

export default new VueRouter
({
    routes: [
        {
            path: '/',
            component: dashboard
        },
        {
            path: '/Patron',
            component: () => import('@/components/router/Patron')
        },
        {
            path: '/Books',
            component: () => import('@/components/router/Books')
        },
        {
            path: '/Settings',
            component: () => import('@/components/router/Settings')
        },

    ]
});