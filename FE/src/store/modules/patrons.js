import axios from "@/config/axios";


export default {
    state: {
      patrons: []
    },
    getters: {
      getPatrons: (state) => state.patrons
    },
    mutations: {
      setPatrons: (state, patrons) => state.patrons = patrons,
      setDeletePatron: (state, id) =>
        (state.patrons = state.patrons.filter((patron) => patron.id !== id)),
      setNewPatron: (state, patron) => state.patrons.unshift({ ...patron }),
      setUpdatePatron: (state, updatedPatron) => {
        const index = state.patrons.findIndex((patron) => patron.id === updatedPatron.id);
        if (index !== -1) {
          state.patrons.splice(index, 1, { ...updatedPatron });
        }
      },
    },
    actions: {
      async createPatron({ commit }, patron) {
        const response = await axios
          .post("/patrons", patron)
          .then((res) => {
            commit("setNewPatron", res.data.patron);
            return res;
          })
          .catch((err) => {
            return err.response;
          });
          return response;
      },
      async deletePatron({ commit }, id) {
        const response = await axios
          .delete(`/patrons/${id}`)
          .then((res) => {
            commit("setDeletePatron", id);
            return res;
          })
          .catch((err) => {
            return err.response;
          });
        return response;
      },
      async fetchPatrons({ commit }) {
        await axios
          .get("/patrons")
          .then((res) => {
            commit("setPatrons", res.data);
          })
          .catch((err) => {
            console.error(err);
          });
          
      },
      async updatePatron({ commit }, {id, patron}) {
        const response = await axios
          .put(`/patrons/${id}`, patron)
          .then((res) => {
            commit("setUpdatePatron", res.data.patron);
            return res;
          })
          .catch((err) => {
            return err.response;
          });
          return response;
      },
    },
  };
  