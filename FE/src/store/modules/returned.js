import axios from "@/config/axios";


export default {

  state: { 
    returned: []
  },
  getters: {},
  mutations: {
    setBorrowedBook: (state, returned) => state.returned.push({ ...returned }),
    setBorrowedBooks: (state, returnedbooks) =>
      (state.returned = returnedbooks),
  },
  actions: {
    async returnedBook({ commit }, returned) {
      const response = await axios
        .post("/returned_books", returned)
        .then((res) => {
          commit("setRetunedBook", res.data.returned);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
        return response;
    },
  },
};
