import axios from "@/config/axios";

export default {
  state: {
    books: [],
  },
  getters: {
    getBooks: (state) => state.books,
  },
  mutations: {
    setBooks: (state, books) => (state.books = books),
    setDeleteBook: (state, id) =>
      (state.books = state.books.filter((book) => book.id !== id)),
    setNewBook: (state, book) => state.books.unshift({ ...book }),
    setUpdateBook: (state, updatedBook) => {
      const index = state.books.findIndex((book) => book.id === updatedBook.id);
      if (index !== -1) {
        state.books.splice(index, 1, { ...updatedBook });
      }
    },
  },
  actions: {
    async createBook({ commit }, book) {
      const response = await axios
        .post("/books", book)
        .then((res) => {
          commit("setNewBook", res.data.book);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async deleteBook({ commit }, id) {
      const response = await axios
        .delete(`/books/${id}`)
        .then((res) => {
          commit("setDeleteBook", id);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async fetchBooks({ commit }) {
      await axios
        .get("/books")
        .then((res) => {
          commit("setBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });   
    },
    async updateBook({ commit }, {id, book}) {
      const response = await axios
        .put(`/books/${id}`, book)
        .then((res) => {   
          commit("setUpdateBook", res.data.book);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
  },
};
