
var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['Jan', 'Fed', 'March', 'April', 'May', 'June','July'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [3, 5, 2, 9, 7, 10, 4],
      backgroundColor: 'rgba(243, 15, 24, .7)',
    },
    {
      label: 'Returned Books',
      data: [6, 2, 1, 11, 1, 3, 5],
      backgroundColor: 'rgba(0, 53, 167, .7)',
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'pie',
  data: {
    datasets: [{
        data: [30, 5, 20, 15],
        backgroundColor: [
          'rgb(243, 15, 24, .7)',
          'rgba(0, 255, 255, .7)',
          'rgba(0, 53, 167, .7)',
          'rgba(234, 4, 221, .7)',
        ],
    }],

    labels: [
        'Math',
        'Science',
        'History',
        'English',
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});


